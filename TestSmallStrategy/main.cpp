
#include "small_strategy/small_strategy.hpp"
#include <iostream>

class IObject
{
public:
	virtual ~IObject() {}
	virtual void DoJob() = 0;
	virtual void clone(IObject* obj) const = 0;
};

class ConcreteObject : public IObject
{
public:
	typedef IObject base;

	ConcreteObject() = default;

	ConcreteObject(const ConcreteObject& object)
	{
		std::cout << "Copy\n";
	}

	ConcreteObject& ConcreteObject::operator=(const ConcreteObject& object)
	{
		std::cout << "Operator=\n";
		return *this;
	}

	virtual void clone(IObject* obj) const override
	{
		new (obj) ConcreteObject (*this);
	}

	virtual void DoJob() override
	{
		std::cout << "Do job\n";
	}
};


int main()
{
	using namespace experimental;

	auto copy = small_strategy<IObject, 32>();

	{
		auto strategy = make_strategy<ConcreteObject, 32>();
		strategy->DoJob();
		copy = strategy;
	}

	copy->DoJob();

	return 0;
}