#include <array>

namespace experimental
{
	template<typename T, size_t NBytes>
	class small_strategy
	{
		typedef small_strategy<T, NBytes> self_type;

	public:
		small_strategy()
		{

		}

		small_strategy(const self_type& other)
		{
			other->clone(get());
		}

		self_type& operator=(const self_type& other)
		{
			std::cout << "Copy_Base\n";
			other->clone(get());
			return *this;
		}

		T* operator->() 
		{
			return reinterpret_cast<T*>(storage_.data());
		}

		const T* operator->() const
		{
			return reinterpret_cast<const T*>(storage_.data());
		}

		T* get() 
		{ // For placement new or delete
			return reinterpret_cast<T*>(storage_.data());
		}

	private:
		std::array<uint8_t, NBytes> storage_;
	};

	template <typename T, size_t N, typename... Args>
	small_strategy<typename T::base, N> make_strategy(const Args&... args)
	{
		small_strategy<T::base, N> result;
		new (result.get()) T(args...);
		return result;
	}

}